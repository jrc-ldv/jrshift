gearshift.core package
======================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   gearshift.core.load
   gearshift.core.model
   gearshift.core.write

Module contents
---------------

.. automodule:: gearshift.core
   :members:
   :undoc-members:
   :show-inheritance:
