gearshift.core.model package
============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   gearshift.core.model.calculateShiftpointsNdvFullPC
   gearshift.core.model.scaleTrace

Module contents
---------------

.. automodule:: gearshift.core.model
   :members:
   :undoc-members:
   :show-inheritance:
