Input File
----------

.. image:: ../doc/_static/images/about_sheet.*

The input file of the GEARSHIFT tool is an excel file, structured in different sheets.

  - **Case sheet:** The case sheet contains a list of cases that the tool will run.
  - **Vehicle sheet:** The vehicle sheet contains a list of vehicles along with their characteristics.
  - **Engine sheet:** The engine sheet contains the vehicle’s full load curves.
  - **Gearbox Ratios sheet:** The gearbox ratios sheet contains the gearbox transmission ratios.

.. include:: ../README.rst
    :start-after: .. _start-usage:
    :end-before: .. _end-usage:

.. include:: ../README.rst
    :start-after: .. _start-sub:
    :end-before: .. _end-sub: