gearshift package
=================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   gearshift.cli
   gearshift.core

Module contents
---------------

.. automodule:: gearshift
   :members:
   :undoc-members:
   :show-inheritance:
